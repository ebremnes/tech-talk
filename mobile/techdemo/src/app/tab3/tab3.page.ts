import { Component } from '@angular/core';
import { StorageService } from '../providers/storage.service';

@Component({
  selector: 'app-tab3',
  templateUrl: 'tab3.page.html',
  styleUrls: ['tab3.page.scss']
})
export class Tab3Page {

  data: any;

  constructor(private storageService: StorageService) {
    this.storageService.deleteDataByKey("report")
      .then(() => this.data = "Data has been deleted")
      .catch(err => console.error(err));
  }

}

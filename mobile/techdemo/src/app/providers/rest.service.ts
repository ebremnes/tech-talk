import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class RestService {

  constructor(private http: Http) { }

  getData(): Observable<any> {
    return this.http.get("http://localhost:3000/data", {});
  }
}

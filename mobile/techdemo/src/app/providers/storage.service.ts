import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';

@Injectable({
  providedIn: 'root'
})
export class StorageService {

  constructor(private storage: Storage) { }

  /**
   * 
   * @param key 
   * @param value 
   */
  async setDataByKeyValue(key: any, value: any) {
    return this.storage.set(key, value);
  }

  /**
   * 
   * @param key 
   */
  async getDataByKey(key: any) {
    return this.storage.get(key);
  }

  /**
   * 
   * @param key 
   */
  async deleteDataByKey(key: any) {
    return this.storage.remove(key);
  }

}

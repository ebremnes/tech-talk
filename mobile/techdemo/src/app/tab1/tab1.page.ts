import { Component } from '@angular/core';
import { StorageService } from '../providers/storage.service';
import { RestService } from '../providers/rest.service';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page {

  data: any;

  constructor(private storageService: StorageService, private rest: RestService) {

    this.rest.getData().subscribe(data => {
      this.storageService.setDataByKeyValue("report", data._body)
        .then(() => this.data = data._body)
        .catch(err => console.error(err))
    });

  }

}

import { Component } from '@angular/core';
import { StorageService } from '../providers/storage.service';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page {

  data: any;
  
  constructor(private storageService: StorageService) {
    this.storageService.getDataByKey("report")
      .then(data => this.data = data)
      .catch(err => console.error(err));
  }

}

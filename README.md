# Demo Ionic Typescript & Express

## La platica
[Tech talk Ionic NodeJs Typescript](https://bitbucket.org/ebremnes/tech-talk/src/master/lib/Tech_Talk_Ionic_NodeJs_Typescript.pdf)

## NPM
https://www.npmjs.com/

## Ionic docs
https://ionicframework.com/docs

## Express server demo link
https://expressjs.com/en/starter/hello-world.html

## Install

Backend: npm install
Ionic: npm install
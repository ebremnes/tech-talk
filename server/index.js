const express = require('express')
const app = express()
const port = 3000

app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Methods", ["GET"]);
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

app.get('/data', (req, res) => res.send({
    name: "demo",
    port: port,
    message: "Hi there, how are you?"
}))
app.listen(port, () => console.log(`Example app listening at http://localhost:${port}`));